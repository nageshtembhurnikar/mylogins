package com.test.nagesh.mylogins;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.test.nagesh.mylogins.ServiceCallbacks.SignUpWithEmailSrvCallback;
import com.test.nagesh.mylogins.Services.SignUpWithEmailSrv;
import com.test.nagesh.mylogins.models.PFUser;

public class SignUpActivity extends AppCompatActivity implements SignUpWithEmailSrvCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Handle click events

    public void signUpClicked(View v) {
        EditText pwd = (EditText) this.findViewById(R.id.lblpwd);
        EditText confirmPwd = (EditText) this.findViewById(R.id.txtConfirmPwd);
        if(pwd.getText().toString().equals(confirmPwd.getText().toString())) {
            EditText name = (EditText) this.findViewById(R.id.txtName);
            EditText email = (EditText) this.findViewById(R.id.txtEmail);
            EditText mobileNumber = (EditText) this.findViewById(R.id.txtMobNo);

            PFUser user = new PFUser();
            user.setName(name.getText().toString());
            user.setEmail(email.getText().toString());
            user.setMobNo(mobileNumber.getText().toString());
            user.setPassword(pwd.getText().toString());
            SignUpWithEmailSrv signUpSrv = new SignUpWithEmailSrv();
            signUpSrv.action(user, this);
        }
        else {
            Toast.makeText(this, "Password and Confirm Password doesn't match.",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void userSignUpCallback(PFUser user) {
        showToast("User Signup Successful");
        startUserHomeActivity();
    }

    public void showToast(String message){
        Toast.makeText(this, message,
                Toast.LENGTH_LONG).show();

    }
    public void startUserHomeActivity(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}
