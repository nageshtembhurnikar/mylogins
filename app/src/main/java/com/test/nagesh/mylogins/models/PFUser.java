package com.test.nagesh.mylogins.models;

import com.parse.ParseUser;

/**
 * Created by NAGESH on 28-02-2016.
 */

public class PFUser {

    String objectId;
    String name;
    String email;
    String password;
    String mobNo;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public PFUser getCurrentUser(){
        PFUser user = new PFUser();
        ParseUser parseUser = ParseUser.getCurrentUser();
        user.email = parseUser.getEmail();
        user.name = parseUser.has(Constant.NAME) ? parseUser.get(Constant.NAME).toString() : "";
        user.mobNo = parseUser.has(Constant.MOBILE_NUMBER) ? parseUser.get(Constant.MOBILE_NUMBER).toString() : "";
        return user;
    }

}
