package com.test.nagesh.mylogins;

import android.app.Application;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;

/**
 * Created by NAGESH on 28-02-2016.
 */
public class DefaultApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this);
        ParseFacebookUtils.initialize(this);
    }
}
