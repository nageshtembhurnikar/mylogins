package com.test.nagesh.mylogins;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseUser;
import com.test.nagesh.mylogins.models.PFUser;

public class HomeActivity extends AppCompatActivity {

    TextView lblShowName;
    TextView lblShowEmail;
    TextView lblShowMobNo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        PFUser user = new PFUser().getCurrentUser();
        lblShowName = (TextView) this.findViewById(R.id.lblShowName);
        lblShowEmail = (TextView) this.findViewById(R.id.lblShowEmail);
        lblShowMobNo = (TextView) this.findViewById(R.id.lblShowMobNo);

        lblShowName.setText(user.getName());
        lblShowEmail.setText(user.getEmail());
        lblShowMobNo.setText(user.getMobNo());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void logoutClicked(View v){
        ParseUser.logOut();
        startLoginActivity();
    }

    public void startLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
