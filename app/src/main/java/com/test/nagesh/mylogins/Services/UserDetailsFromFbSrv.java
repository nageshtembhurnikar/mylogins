package com.test.nagesh.mylogins.Services;
import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.test.nagesh.mylogins.LoginActivity;
import com.test.nagesh.mylogins.ServiceCallbacks.UserDetailFromFbSrvCallback;
import com.test.nagesh.mylogins.models.PFUser;

import org.json.JSONException;

/**
 * Created by NAGESH on 29-02-2016.
 */
public class UserDetailsFromFbSrv {
    public void action(final UserDetailFromFbSrvCallback callback){

        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,name");
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                parameters,
                HttpMethod.GET,
                new GraphRequest.Callback() {

                    public void onCompleted(GraphResponse response) {
                        /* handle the result */
                        try {
                            PFUser user = new PFUser();
                            user.setEmail(response.getJSONObject().getString("email"));
                            user.setName(response.getJSONObject().getString("name"));
                            callback.userDetailFromFbCallback(user);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }
}
