package com.test.nagesh.mylogins;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.test.nagesh.mylogins.Services.SignUpWithEmailSrv;
import com.test.nagesh.mylogins.models.Constant;
import com.test.nagesh.mylogins.models.PFUser;

public class SetPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_set_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startUserHomeActivity(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    public void saveClicked(View v){
        EditText password = (EditText) this.findViewById(R.id.txtPwd);
        EditText confirmPassword = (EditText) this.findViewById(R.id.txtConfirmPwd);

        if(password.getText().toString().equals(confirmPassword.getText().toString())) {
            ParseUser parseUser = ParseUser.getCurrentUser();
            parseUser.setPassword(password.getText().toString());
            parseUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    startUserHomeActivity();
                }
            });
        }
        else {
            Toast.makeText(this, "Password and Confirm Password doesn't match.",
                    Toast.LENGTH_LONG).show();
        }
    }
}
