package com.test.nagesh.mylogins;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.test.nagesh.mylogins.ServiceCallbacks.*;
import com.test.nagesh.mylogins.Services.*;
import com.test.nagesh.mylogins.models.Constant;
import com.test.nagesh.mylogins.models.PFUser;
import java.security.MessageDigest;
import java.util.Arrays;


import java.util.List;

public class LoginActivity extends AppCompatActivity implements UserDetailFromFbSrvCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    // Handle button clicks

    public void logInClicked(View v){
        EditText email = (EditText) this.findViewById(R.id.txtEmail);
        EditText pwd = (EditText) this.findViewById(R.id.lblpwd);

        String strEmail = email.getText().toString();
        String strPwd = pwd.getText().toString();
        ParseUser.logInInBackground(strEmail, strPwd, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if(user != null){
                    startUserHomeActivity();
                }
                else {
                    showToast("Invalid Login Credentials");
                }
            }
        });

    }
    public void loginWithFbClicked(View v){
        final List<String> mPermissions = Arrays.asList("public_profile", "email");
        ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginActivity.this, mPermissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    Log.d("MyApp", "Error Occured For Facebook login.");
                } else if (user.isNew()) {
                    Log.d("MyApp", "User signed up and logged in through Facebook!");
                    getUserDetailWithFb();
                } else {
                    Log.d("MyApp", "User logged in through Facebook!");
                    startUserHomeActivity();
                }
            }
        });
    }
    public void loginWithGPlusClicked(View v){

    }
    public void signUpClicked(View v){
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    public void getUserDetailWithFb(){
        UserDetailsFromFbSrv userDetailSrv = new UserDetailsFromFbSrv();
        userDetailSrv.action(this);
    }

    public void startSetPasswordActivity( ) {
        Intent intent = new Intent(this, SetPasswordActivity.class);
        startActivity(intent);
    }

    public void startUserHomeActivity(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    public void showToast(String message){
        Toast.makeText(this, message,
                Toast.LENGTH_LONG).show();
    }

    // CallBacks
    @Override
    public void userDetailFromFbCallback(PFUser user) {
        // Getting Only Name and email from facebook
        ParseUser parseUser = ParseUser.getCurrentUser();
        parseUser.setEmail(user.getEmail());
        parseUser.setUsername(user.getEmail());
        if(user.getName() != null)
            parseUser.put(Constant.NAME, user.getName());
        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                startSetPasswordActivity();
            }
        });
    }
}
