package com.test.nagesh.mylogins.Services;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.test.nagesh.mylogins.ServiceCallbacks.SignUpWithEmailSrvCallback;
import com.test.nagesh.mylogins.models.Constant;
import com.test.nagesh.mylogins.models.PFUser;

/**
 * Created by NAGESH on 28-02-2016.
 */
public class SignUpWithEmailSrv {

    public void action(final PFUser user, final SignUpWithEmailSrvCallback callback){
        final ParseUser parseUser = new ParseUser();
        parseUser.setUsername(user.getEmail());
        parseUser.setPassword(user.getPassword());
        parseUser.setEmail(user.getEmail());
        parseUser.put(Constant.NAME, user.getName());
        parseUser.put(Constant.MOBILE_NUMBER, user.getMobNo());

        parseUser.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Success!
                    String objectId = parseUser.getObjectId();
                    user.setObjectId(parseUser.getObjectId());
                    callback.userSignUpCallback(user);

                } else {
                    // Failure!
                    callback.userSignUpCallback(null);
                }
            }
        });
    }
}
