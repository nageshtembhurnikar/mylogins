package com.test.nagesh.mylogins.ServiceCallbacks;
import com.test.nagesh.mylogins.models.PFUser;

/**
 * Created by NAGESH on 29-02-2016.
 */
public interface UserDetailFromFbSrvCallback {
    public void userDetailFromFbCallback(PFUser user);
}