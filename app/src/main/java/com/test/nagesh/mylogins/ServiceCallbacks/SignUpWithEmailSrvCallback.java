package com.test.nagesh.mylogins.ServiceCallbacks;
import com.test.nagesh.mylogins.models.PFUser;

/**
 * Created by NAGESH on 28-02-2016.
 */
public interface SignUpWithEmailSrvCallback {
    public void userSignUpCallback(PFUser user );
}
